//активность меню при скроле
$(document).scroll(function() {
  let scrollTop = $(document).scrollTop();
  let navBar = $('.header li');
  navBar.each(function(i,elem) {
    let elemAttr = $(elem).find('a').attr('href');
    if(scrollTop >= $(elemAttr).offset().top - $(window).height() / 2) {
      navBar.removeClass('active');
      $(elem).addClass('active');
    }
    if(scrollTop >= $(elemAttr).offset().top + $(elemAttr).outerHeight() - $(window).height() / 2) {
      navBar.removeClass('active');
    }
  });
});





// список выпадающего меню
let liEach = $('.header li');
let ulWidth = $('.header ul').width();


$(document).ready(function() {
  ret();
});
$('.more').on('click', function() {
  $(this).toggleClass('active');
  if($(this).hasClass('active')) {
    liEach.fadeIn();
  } else{
    ret();
  }
});


function ret() {
  var summ = 0;
  liEach.each(function(i,elem) {
    summ += $(elem).width();
    if(summ >= ulWidth) {
      $(elem).hide();
    }
  });
}